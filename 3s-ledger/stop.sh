#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

docker kill $(docker ps -q)

docker rm $(docker ps -a -q)

# Shut down the Docker containers that might be currently running.
docker-compose -f docker-compose.yml stop
