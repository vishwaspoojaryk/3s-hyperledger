#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1


docker-compose -f docker-compose.yml down

docker-compose -f docker-compose.yml up -d ca.spectrus.com ca.slave-spectrus.com 3s.com peer0.master.spectrus.com  peer0.slave.spectrus.com   
docker ps -a

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=10
#echo ${FABRIC_START_TIMEOUT}
sleep ${FABRIC_START_TIMEOUT}

# Create the channel
docker exec -e "CORE_PEER_LOCALMSPID=Spectrus" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@master.spectrus.com/msp" peer0.master.spectrus.com peer channel create -o 3s.com:7050 -c ssschannel -f /etc/hyperledger/configtx/channel.tx


docker exec -e "CORE_PEER_LOCALMSPID=Spectrus" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@master.spectrus.com/msp" -e "CORE_PEER_COMMITTER_LEDGER_ORDERER=3s.com:7050" -e "CORE_PEER_ADDRESS=peer0.master.spectrus.com:7051" peer0.master.spectrus.com peer channel join -b ssschannel.block

docker exec -e "CORE_PEER_LOCALMSPID=Slave-Spectrus" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@slave.spectrus.com/msp" -e "CORE_PEER_COMMITTER_LEDGER_ORDERER=3s.com:7050" -e "CORE_PEER_ADDRESS=peer0.slave.spectrus.com:7051" peer0.slave.spectrus.com peer channel fetch oldest ssschannel.block -c ssschannel -o 3s.com:7050

docker exec -e "CORE_PEER_LOCALMSPID=Slave-Spectrus" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@slave.spectrus.com/msp" -e "CORE_PEER_COMMITTER_LEDGER_ORDERER=3s.com:7050" -e "CORE_PEER_ADDRESS=peer0.slave.spectrus.com:7051" peer0.slave.spectrus.com peer channel join -b ssschannel.block

