/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const FabCar = require('./lib/fabcar');
const Product3s = require('./lib/Product3s');

module.exports.FabCar = FabCar;
module.exports.Product3s = Product3s;
module.exports.contracts = [ Product3s ];
